// file="index.html"
// Parts of the code are copied from http://multivis.net/lecture/phong.html
uniform lowp float u_ka;
uniform lowp float u_kd;
uniform lowp float u_ks;

uniform lowp vec3 u_cameraPosition;
uniform lowp vec3 u_lightDirection;

uniform lowp vec3 u_ambientColor;
uniform lowp vec3 u_specularColor;

varying lowp vec3 v_color;
varying lowp vec3 v_normal;


void main(void) {
    lowp float shininess = 1.;

    lowp vec3 diffuseColor = v_color;

    lowp vec3 N = normalize(v_normal);
    lowp vec3 L = normalize(u_lightDirection);
    lowp float lambertian = max(dot(N, L), 0.0);
    lowp float specAngle = 0.0;

    if(lambertian > 0.0) {
        lowp vec3 R = reflect(-L, N);
        lowp vec3 V = normalize(-u_cameraPosition);

        specAngle = max(dot(R, V), 0.0);
    }

    gl_FragColor = vec4(u_ka * u_ambientColor +
                        u_kd * lambertian * diffuseColor +
                        u_ks * specAngle * u_specularColor, 1.0);
}