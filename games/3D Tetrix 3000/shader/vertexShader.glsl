// file="index.html"    
attribute vec4 a_vertexPosition;
attribute vec4 a_vertexColor;
attribute vec4 a_vertexNormal;
uniform mat4 u_modelViewMatrix;
uniform mat4 u_projectionMatrix;

varying lowp vec3 v_color;
varying lowp vec3 v_normal;
varying lowp vec3 v_pos;

void main(void) {
    gl_Position = u_projectionMatrix * u_modelViewMatrix * a_vertexPosition;
    
    v_color = a_vertexColor.xyz;
    v_normal = a_vertexNormal.xyz;
    v_pos = a_vertexPosition.xyz;
}