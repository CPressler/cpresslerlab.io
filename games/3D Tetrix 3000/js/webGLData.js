//WebGLData is responsible for webcontext handling and contains mostly boilerplate code from the tutorium and mozilla
class WebGLData {
    constructor(context, vertexShaderSource, fragmentShaderSource, modelViewMatrix, projectionMatrix, cameraPosition, ambient, diffuse, specular) {
        this.debug = 1;
        this.context = context;

        let shaderProgram = this.initShaderProgram(vertexShaderSource, fragmentShaderSource);

        this.programInfo = {
            program: shaderProgram,
            a_vertexPosition: this.context.getAttribLocation(shaderProgram, "a_vertexPosition"),
            a_vertexColor: this.context.getAttribLocation(shaderProgram, "a_vertexColor"),
            a_vertexNormal: this.context.getAttribLocation(shaderProgram, "a_vertexNormal"),

            u_projectionMatrix: this.context.getUniformLocation(shaderProgram, 'u_projectionMatrix'),
            u_modelViewMatrix: this.context.getUniformLocation(shaderProgram, 'u_modelViewMatrix'),

            u_ka: this.context.getUniformLocation(shaderProgram, 'u_ka'),
            u_kd: this.context.getUniformLocation(shaderProgram, 'u_kd'),
            u_ks: this.context.getUniformLocation(shaderProgram, 'u_ks'),

            u_ambientColor: this.context.getUniformLocation(shaderProgram, 'u_ambientColor'),
            u_specularColor: this.context.getUniformLocation(shaderProgram, 'u_specularColor'),

            u_lightDirection: this.context.getUniformLocation(shaderProgram, 'u_lightDirection'),
            u_cameraPosition: this.context.getUniformLocation(shaderProgram, 'u_cameraPosition'),
        };

        this.initializeLight();

        this.positionBuffer = this.context.createBuffer();
        this.colorBuffer = this.context.createBuffer();
        this.normalBuffer = this.context.createBuffer();

        this.context.useProgram(this.programInfo.program);
        this.updateViewing(modelViewMatrix, projectionMatrix, cameraPosition)
        this.updateMaterials(ambient, diffuse, specular)
    }

    initShaderProgram(vertexShaderSource, fragmentShaderSource) {
        let vertexShader = this.loadShader(this.context.VERTEX_SHADER, vertexShaderSource);
        let fragmentShader = this.loadShader(this.context.FRAGMENT_SHADER, fragmentShaderSource);

        let shaderProgram = this.context.createProgram();
        this.context.attachShader(shaderProgram, vertexShader);
        this.context.attachShader(shaderProgram, fragmentShader);
        this.context.linkProgram(shaderProgram);

        if (!this.context.getProgramParameter(shaderProgram, this.context.LINK_STATUS)) {
            throw new Error("Unable to initialize shader program, info log: " + this.context.getProgramInfoLog(shaderProgram));
        }

        return shaderProgram;
    }

    loadShader(shaderType, shaderSource) {
        let shader = this.context.createShader(shaderType);
        this.context.shaderSource(shader, shaderSource);
        this.context.compileShader(shader);

        if (!this.context.getShaderParameter(shader, this.context.COMPILE_STATUS)) {
            throw new Error("Error while compiling shader, info log: " + this.context.getShaderInfoLog(shader));
        }

        return shader;
    }

    initializeLight(){
        this.context.useProgram(this.programInfo.program);

        this.ambientColor = vec3.fromValues(1, 1, 1);
        this.specularColor = vec3.fromValues(1, 1, 1);
        this.lightDirection = vec3.fromValues(1, 1, 1);

        this.context.uniform3fv(this.programInfo.u_ambientColor, this.ambientColor);
        this.context.uniform3fv(this.programInfo.u_specularColor, this.specularColor);
        this.context.uniform3fv(this.programInfo.u_lightDirection, this.lightDirection)
    }

    updateViewing(modelViewMatrix, projectionMatrix, cameraPosition){        
        this.context.useProgram(this.programInfo.program);

        this.context.uniformMatrix4fv(
            this.programInfo.u_projectionMatrix,
            false,
            projectionMatrix);
        this.context.uniformMatrix4fv(
            this.programInfo.u_modelViewMatrix,
            false,
            modelViewMatrix);

        this.context.uniform3fv(this.programInfo.u_cameraPosition, cameraPosition);
    }

    updateMaterials(ambient, diffuse, specular){
        this.context.useProgram(this.programInfo.program);

        this.context.uniform1f(this.programInfo.u_ka, ambient);
        this.context.uniform1f(this.programInfo.u_kd, diffuse);
        this.context.uniform1f(this.programInfo.u_ks, specular);
    }

    updateBuffers(data) {
        this.context.bindBuffer(this.context.ARRAY_BUFFER, this.positionBuffer);
        this.context.bufferData(this.context.ARRAY_BUFFER, new Float32Array(data.positions), this.context.STATIC_DRAW);

        this.context.bindBuffer(this.context.ARRAY_BUFFER, this.colorBuffer);
        this.context.bufferData(this.context.ARRAY_BUFFER, new Float32Array(data.colors), this.context.STATIC_DRAW);

        this.context.bindBuffer(this.context.ARRAY_BUFFER, this.normalBuffer);
        this.context.bufferData(this.context.ARRAY_BUFFER, new Float32Array(data.normals), this.context.STATIC_DRAW);

        return {
            position: this.positionBuffer,
            color: this.colorBuffer,
            normal: this.normalBuffer,
        };
    }

    drawTriangles(buffers, vertexCount) {
        this.context.clearColor(0, 0, 0, 1);
        this.context.clearDepth(1.0);
        this.context.enable(this.context.DEPTH_TEST);

        this.context.clear(this.context.COLOR_BUFFER_BIT | this.context.DEPTH_BUFFER_BIT);

        let vertNumComponents = 3;
        let vertType = this.context.FLOAT;
        let vertNormalize = false;
        let vertStride = 0;
        let vertDataOffset = 0;
        this.setShaderData(buffers.position, this.programInfo.a_vertexPosition, vertNumComponents, vertType, vertNormalize, vertStride, vertDataOffset);

        let fragNumComponents = 4;
        let fragType = this.context.FLOAT;
        let fragNormalize = false;
        let fragStride = 0;
        let fragDataOffset = 0;
        this.setShaderData(buffers.color, this.programInfo.a_vertexColor, fragNumComponents, fragType, fragNormalize, fragStride, fragDataOffset);

        let normNumComponents = 3;
        let normType = this.context.FLOAT;
        let normNormalize = false;
        let normStride = 0;
        let normDataOffset = 0;
        this.setShaderData(buffers.normal, this.programInfo.a_vertexNormal, normNumComponents, normType, normNormalize, normStride, normDataOffset);

        this.context.useProgram(this.programInfo.program);

        let drawOffset = 0;
        this.context.drawArrays(this.context.TRIANGLES, drawOffset, vertexCount);
    }

    drawLines(buffers, vertexCount){
        let vertNumComponents = 3;
        let vertType = this.context.FLOAT;
        let vertNormalize = false;
        let vertStride = 0;
        let vertDataOffset = 0;
        this.setShaderData(buffers.position, this.programInfo.a_vertexPosition, vertNumComponents, vertType, vertNormalize, vertStride, vertDataOffset);

        let fragNumComponents = 4;
        let fragType = this.context.FLOAT;
        let fragNormalize = false;
        let fragStride = 0;
        let fragDataOffset = 0;
        this.setShaderData(buffers.color, this.programInfo.a_vertexColor, fragNumComponents, fragType, fragNormalize, fragStride, fragDataOffset);

        this.context.useProgram(this.programInfo.program);

        let drawOffset = 0;
        this.context.drawArrays(this.context.LINES, drawOffset, vertexCount);
    }

    setShaderData(bufferData, attributeData, numComponents, type, normalize, stride, dataOffset) {
        this.context.bindBuffer(this.context.ARRAY_BUFFER, bufferData);
        this.context.vertexAttribPointer(attributeData, numComponents, type, normalize, stride, dataOffset);
        this.context.enableVertexAttribArray(attributeData);
    }
}