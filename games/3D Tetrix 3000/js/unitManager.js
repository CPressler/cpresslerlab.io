class UnitManager {
    constructor(unitSizes, aspectRatio) {
        this.aspectRation = aspectRatio;
        this.setUnitSize(unitSizes);
    }
    setUnitSize(unitSize){
        this.unitSize = {
            x: unitSize[0],
            y: unitSize[1],
            z: unitSize[2],
        };
        this.gridSize = this.toGridCoordinates3D({
            x: 1,
            y: 1,
            z: 1,
        });
        this.unitDimensions = vec3.fromValues(2 / this.unitSize.x, 2 / this.unitSize.y, 2 / this.unitSize.z);
    }
    //returns the top center position for one tetris block (relevant for later assignments and the actual game)
    getTopCenter() {
        return vec3.fromValues(0, this.gridSize.y - 0.5, 0);
    }
    //returns webgl coordinates as grid (=unit) coordinates
    toGridCoordinates3D(coordinates3D) {
        return {
            x: coordinates3D.x / this.unitSize.x,
            y: coordinates3D.y / this.unitSize.y,
            z: coordinates3D.z / this.unitSize.z,
        };
    }
    //returns grid (=unit) coordingates as webgl coordinates
    toWebGLCoordinates3D(coordinates3D) {
        return {
            x: coordinates3D.x * this.unitSize.x,
            y: coordinates3D.y * this.unitSize.y,
            z: coordinates3D.z * this.unitSize.z,
        };
    }
    //returns an array of grid (=unit) coordinates to webgl coordinates
    toWebGLCoordinates3DArray(coordinatesArray) {
        let currentCoordinateIndex = 0;
        for (let i = 0; i < coordinatesArray.length; i += 3) {
            coordinatesArray[currentCoordinateIndex++] *= this.unitSize.x;
            coordinatesArray[currentCoordinateIndex++] *= this.unitSize.y;
            coordinatesArray[currentCoordinateIndex++] *= this.unitSize.z;
        }
        return coordinatesArray;
    }

    createGrid(advancedGrid) {
        let gridData = {
            positions: new Array(),
            colors: new Array(),
            normals: new Array(),
            vertexCount: 0,
        }

        for (let i = 0; i < 3; i++) {
            for (let ii = 0; ii < 3; ii++) {
                if (i == ii) {
                    continue;
                }
                if(advancedGrid){
                    let relevantUnitSize = 0;    

                    switch (ii) {
                        case 0:
                            relevantUnitSize = this.unitSize.x;
                            break;
                        case 1:
                            relevantUnitSize = this.unitSize.x;
                            break;
                        case 2:
                            relevantUnitSize = this.unitSize.x;
                            break;
                    }
                    let smallGridCount = (1 / relevantUnitSize) * 2 + 1;
                    for(let iii = 0; iii < smallGridCount; iii++){
                        let offset = relevantUnitSize * iii
                        let newLinePositions = this.createLines(i, ii, -1 + offset);
                        gridData.positions = gridData.positions.concat(newLinePositions);

                        let color = new Array();
                        for(let iiii = 0; iiii < newLinePositions.length; iiii++){
                            gridData.colors = gridData.colors.concat([1, 1, 1, 1]);
                        }
                    }
                } else{
                    gridData.positions = gridData.positions.concat(this.createLines(i, ii, -1));
                }
            }
        }

        gridData.vertexCount += gridData.positions.length / 3;
        gridData.colors.length = gridData.vertexCount * 4;
        gridData.colors.fill(1);
        gridData.normals.length = gridData.positions.length;
        gridData.normals.fill(1);

        return gridData;
    }

    createLines(movingAxis, fixedAxis, fixedValue) {
        let positionArray = new Array();
        let lineCount = 0;

        switch (movingAxis) {
            case 0:
                lineCount = (1 / this.unitSize.x) * 2 + 1;
                switch (fixedAxis) {
                    case 1:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.x * i;
                            positionArray = positionArray.concat([-1 + offset, fixedValue, -1, -1 + offset, fixedValue, 1]);
                        }
                        break;
                    case 2:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.x * i;
                            positionArray = positionArray.concat([-1 + offset, -1, fixedValue, -1 + offset, 1, fixedValue]);
                        }
                        break;
                }
                break;
            case 1:
                lineCount = (1 / this.unitSize.y) * 2 + 1;
                switch (fixedAxis) {
                    case 0:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.y * i;
                            positionArray = positionArray.concat([fixedValue, -1 + offset, -1, fixedValue, -1 + offset, 1]);
                        }
                        break;
                    case 2:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.y * i;
                            positionArray = positionArray.concat([-1, -1 + offset, fixedValue, 1, -1 + offset, fixedValue]);
                        }
                        break;
                }
                break;
            case 2:
                lineCount = (1 / this.unitSize.z) * 2 + 1;
                switch (fixedAxis) {
                    case 0:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.z * i;
                            positionArray = positionArray.concat([fixedValue, -1, -1 + offset, fixedValue, 1, -1 + offset]);
                        }
                        break;
                    case 1:
                        for (let i = 0; i < lineCount; i++) {
                            let offset = this.unitSize.z * i;
                            positionArray = positionArray.concat([-1, fixedValue, -1 + offset, 1, fixedValue, -1 + offset]);
                        }
                        break;
                }
                break;
        }

        return positionArray;
    }
}