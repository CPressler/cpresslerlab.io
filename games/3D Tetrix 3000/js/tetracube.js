class Tetracube {
    constructor(tetracubeManager, tetracubeType, cubes, rotationCubeIndex, rotationCubeVertexIndex){
        this.tetracubeManager = tetracubeManager;
        this.tetracubeType = tetracubeType;
        this.cubes = cubes;
        this.rotationCubeIndex = rotationCubeIndex;
        this.rotationCubeVertexIndex = rotationCubeVertexIndex;
        this.makeCopy();
    }

    getData(){
        let data = {
            positions: [],
            colors: [],
            normals: [],
            vertexCount: 0
        }

        for(let i = 0; i < this.cubes.length; i++){
            let cubeData = this.cubes[i].getData();
            let cubeCopyData = this.cubeCopy[i].getData();

            
            data.positions = data.positions.concat(cubeData.positions);
            data.colors = data.colors.concat(cubeData.colors);
            data.normals = data.normals.concat(cubeData.normals);
            
            /*
            data.positions = data.positions.concat(cubeCopyData.positions);
            data.colors = data.colors.concat(cubeCopyData.colors);
            data.normals = data.normals.concat(cubeCopyData.normals);
            data.vertexCount += data.positions.length / 3;
            */
        }

        data.vertexCount = data.positions.length / 3;

        return data;
    }

    applyTransformationMatrix(matrix, rotationMatrix, test){
        if(test){
            for(let i = 0; i < this.cubeCopy.length; i++){
                this.cubeCopy[i].applyTransformationMatrix(matrix, rotationMatrix);
            }
            return;
        }
        else{
            for(let i = 0; i < this.cubes.length; i++){
                this.cubes[i].applyTransformationMatrix(matrix, rotationMatrix);
            }
            return;
        }
    }

    finishRotation(missingTranslation){
        let translationMatrix = mat4.fromTranslation(mat4.create(), missingTranslation);
        for(let i = 0; i < this.cubeCopy.length; i++){
            this.cubeCopy[i].applyTransformationMatrix(translationMatrix, mat4.create(), false);
        }        
        for(let i = 0; i < this.cubeCopy.length; i++){
            this.cubes[i].positions = this.cubeCopy[i].positions.slice();
            this.cubes[i].normals = this.cubeCopy[i].normals.slice();
        }
        let backTranslationMatrix = mat4.fromTranslation(mat4.create(), vec3.negate(vec3.create(), missingTranslation));
        for(let i = 0; i < this.cubeCopy.length; i++){
            this.cubeCopy[i].applyTransformationMatrix(backTranslationMatrix, mat4.create(), false);
        }
    }

    getRotationCenter(){
        return this.cubeCopy[this.rotationCubeIndex].getPoint(this.rotationCubeVertexIndex);
    }

    makeCopy(){
        this.cubeCopy = new Array();
        for(let i = 0; i < this.cubes.length; i++){
            this.cubeCopy.push(new Cube(this.cubes[i].positions.slice(), new Array((this.cubes[i].positions.length / 3) * 4).fill(1), this.cubes[i].normals.slice()));
        }
    }

    getCollisionMatrixIndices(unitDimensions){
        let collisionMatrixIndices = new Array();
        let pointOneStartIndex = 5 * 3;
        let pointTwoStartIndex = 8 * 3;

        for(let i = 0; i < this.cubeCopy.length; i++){
            let pointOne = vec3.fromValues(this.cubeCopy[i].positions[pointOneStartIndex], this.cubeCopy[i].positions[pointOneStartIndex + 1], this.cubeCopy[i].positions[pointOneStartIndex + 2]);
            let pointTwo = vec3.fromValues(this.cubeCopy[i].positions[pointTwoStartIndex], this.cubeCopy[i].positions[pointTwoStartIndex + 1], this.cubeCopy[i].positions[pointTwoStartIndex + 2]);

            let centerPoint = vec3.lerp(vec3.create(), pointOne, pointTwo, 0.5);
            
            let xIndex = Math.round(centerPoint[0]) + Math.floor(unitDimensions[0] / 2);
            let yIndex = Math.round(centerPoint[1] * -1) + Math.floor(unitDimensions[1] / 2);
            let zIndex = Math.round(centerPoint[2]) + Math.floor(unitDimensions[2] / 2);

            collisionMatrixIndices.push(vec3.fromValues(xIndex, yIndex, zIndex));            
        }

        return collisionMatrixIndices;
    }

    clearSlice(y, unitDimensions){
        let indices = this.getCollisionMatrixIndices(unitDimensions);
        for(let i = 0; i < indices.length; i++){
            if(indices[i][1] == y){
                this.cubes[i].removeData();
                this.cubeCopy[i].removeData();
            }
            else if(indices[i][1] < y){
                let translationMatrix = mat4.fromTranslation(mat4.create(), vec3.fromValues(0, -1, 0));
                this.cubes[i].applyTransformationMatrix(translationMatrix, mat4.create());
                this.cubeCopy[i].applyTransformationMatrix(translationMatrix, mat4.create());
            }
        }
    }
}