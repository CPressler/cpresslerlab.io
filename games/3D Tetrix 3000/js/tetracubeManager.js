class TetracubeManager {
    constructor(gameManager) {
        this.gameManager = gameManager;
        this.fixedTetracubes = new Array();
        this.lastTetracubeTypes = new Array();

        this.collisionMatrix = this.initializeCollisionMatrix();
        this.movingTetracubeCollisionMatrixPositions = new Array();
    }

    initializeCollisionMatrix() {
        let matrix = new Array(this.gameManager.unitManager.unitDimensions[0]);
        for (let i = 0; i < matrix.length; i++) {
            matrix[i] = new Array(this.gameManager.unitManager.unitDimensions[1]);
            for (let ii = 0; ii < matrix[i].length; ii++) {
                matrix[i][ii] = new Array(this.gameManager.unitManager.unitDimensions[2]).fill(0);
            }
        }
        return matrix;
    }

    changeCollisionMatrixValues(indices, value) {
        let returnValue = true;
        for (let i = 0; i < indices.length; i++) {
            if (this.collisionMatrix[indices[i][0]][indices[i][1]][indices[i][2]] >= 2) {
                returnValue = false;
            }
            this.collisionMatrix[indices[i][0]][indices[i][1]][indices[i][2]] += value;
        }
        return returnValue;
    }

    getTetracubeData() {
        let allTetracubeData = {
            positions: new Array(),
            colors: new Array(),
            normals: new Array(),
            vertexCount: 0,
        }

        let movingTetracubeData = this.movingTetracube.getData();

        allTetracubeData.positions = this.gameManager.unitManager.toWebGLCoordinates3DArray(movingTetracubeData.positions);
        allTetracubeData.colors = movingTetracubeData.colors;
        allTetracubeData.normals = movingTetracubeData.normals;
        allTetracubeData.vertexCount = movingTetracubeData.vertexCount;

        for (let i = 0; i < this.fixedTetracubes.length; i++) {
            let fixedTetracubeData = this.fixedTetracubes[i].getData();
            allTetracubeData.positions = allTetracubeData.positions.concat(this.gameManager.unitManager.toWebGLCoordinates3DArray(fixedTetracubeData.positions));
            allTetracubeData.colors = allTetracubeData.colors.concat(fixedTetracubeData.colors);
            allTetracubeData.normals = allTetracubeData.normals.concat(fixedTetracubeData.normals);
            allTetracubeData.vertexCount += fixedTetracubeData.vertexCount;
        }

        return allTetracubeData;
    }

    registerInCollisionMatrix(tetracube) {
        this.movingTetracube = tetracube;
        let indices = this.movingTetracube.getCollisionMatrixIndices(this.gameManager.unitManager.unitDimensions);
        this.movingTetracubeCollisionMatrixIndices = indices;
        if(this.checkIfValid(indices)){
            return this.changeCollisionMatrixValues(indices, 1);
        }
        return false;
    }

    makeCollisionMovement(movement) {
        let success = true;
        let transformationMatrix = mat4.fromTranslation(mat4.create(), movement);
        this.movingTetracube.applyTransformationMatrix(transformationMatrix, mat4.create(), true);

        let newIndices = this.movingTetracube.getCollisionMatrixIndices(this.gameManager.unitManager.unitDimensions);

        if (this.checkIfValid(newIndices)) {
            this.updateCollisionMatrix(newIndices, 1);
            success = true;
        }
        else {
            success = false;
        }

        let backTransformationMatrix = mat4.fromTranslation(mat4.create(), vec3.negate(vec3.create(), movement));
        this.movingTetracube.applyTransformationMatrix(backTransformationMatrix, mat4.create(), true);

        return success;
    }

    makeRotationMovement(rotationAxis) {
        let rotationCenter = this.movingTetracube.getRotationCenter();

        let translateToCenterM = mat4.fromTranslation(mat4.create(), vec3.negate(vec3.create(), rotationCenter));
        let rotationM = mat4.fromRotation(mat4.create(), 90 * Math.PI / 180, rotationAxis);
        let translateToTargetM = mat4.fromTranslation(mat4.create(), rotationCenter);

        let transformationMatrix = mat4.multiply(mat4.create(), translateToTargetM, mat4.multiply(mat4.create(), rotationM, translateToCenterM));

        this.movingTetracube.applyTransformationMatrix(transformationMatrix, rotationM, true);

        let newIndices = this.movingTetracube.getCollisionMatrixIndices(this.gameManager.unitManager.unitDimensions);

        if (this.checkIfValid(newIndices)) {
            this.updateCollisionMatrix(newIndices, 1);
            return true;
        }
        else {
            let inversRotationM = mat4.fromRotation(mat4.create(), -90 * Math.PI / 180, rotationAxis);
            let inverseTransformationMatrix = mat4.multiply(mat4.create(), translateToTargetM, mat4.multiply(mat4.create(), inversRotationM, translateToCenterM));

            this.movingTetracube.applyTransformationMatrix(inverseTransformationMatrix, inversRotationM, true);
            return false;
        }
    }

    updateCollisionMatrix(newIndices, newValue) {
        this.changeCollisionMatrixValues(this.movingTetracubeCollisionMatrixIndices, -1);
        this.changeCollisionMatrixValues(newIndices, newValue);
        this.movingTetracubeCollisionMatrixIndices = newIndices;
    }

    checkIfValid(indices) {
        for (let i = 0; i < indices.length; i++) {
            if (!this.checkIfInCollisionMatrix(indices[i]) || this.collisionMatrix[indices[i][0]][indices[i][1]][indices[i][2]] >= 2) {
                return false;
            }
        }
        return true;
    }

    checkIfInCollisionMatrix(index) {
        if (index[0] < 0 || index[0] >= this.collisionMatrix.length)
            return false;
        if (index[1] < 0 || index[1] >= this.collisionMatrix[0].length)
            return false;
        if (index[2] < 0 || index[2] >= this.collisionMatrix[0][0].length)
            return false;
        return true;
    }

    fixTetracube() {
        this.updateCollisionMatrix(this.movingTetracube.getCollisionMatrixIndices(this.gameManager.unitManager.unitDimensions), 2);
        this.fixedTetracubes.push(this.movingTetracube);
        this.emptyFullSlices();
    }

    getSpawnableType() {
        let tetracubeType;
        let typePossible = false;
        while (!typePossible) {
            tetracubeType = Math.round(Math.random() * 6) + 1;
            let tetracubeCount = 0;
            for (let i = 0; i < this.lastTetracubeTypes.length; i++) {
                if (this.lastTetracubeTypes[i] == tetracubeType) {
                    tetracubeCount++;
                }
            }
            if (tetracubeCount <= 1) {
                typePossible = true;
            }
        }
        this.lastTetracubeTypes.unshift(tetracubeType);
        this.lastTetracubeTypes.length = 10;
        return tetracubeType;
    }

    printCollisionMatrix() {
        for (let i = 0; i < this.collisionMatrix.length; i++) {
            for (let ii = 0; ii < this.collisionMatrix[i].length; ii++) {
                for (let iii = 0; iii < this.collisionMatrix[i][ii].length; iii++) {
                    let collisionMatrixValue = this.collisionMatrix[i][ii][iii];
                    if (collisionMatrixValue != 0) {
                        console.log(i + "/" + ii + "/" + iii + ", Value " + collisionMatrixValue);
                    }
                }
            }
        }
    }

    //moves the tetracube to bottom
    moveToBottom() {
        let cubesToJump = 0;
        while (!this.checkIfBottom()) {
            cubesToJump++;
            if(cubesToJump > 100){
                break;
            }
        }

        cubesToJump--;        
        let transformationMatrix = mat4.fromTranslation(mat4.create(), vec3.fromValues(0, -cubesToJump, 0));
        this.movingTetracube.applyTransformationMatrix(transformationMatrix, mat4.create(), false);
    }

    //checks if the currently tetracube hit the bottom
    checkIfBottom(){
        let firstMove = this.makeCollisionMovement(vec3.fromValues(0, -1, 0));
        let success = this.makeCollisionMovement(vec3.fromValues(0, -1, 0));
        if(success){
            let transformationMatrix = mat4.fromTranslation(mat4.create(), vec3.fromValues(0, -1, 0));
            this.movingTetracube.applyTransformationMatrix(transformationMatrix, mat4.create(), true);
        } else{
            let transformationMatrix = mat4.fromTranslation(mat4.create(), vec3.fromValues(0, 1, 0));
            this.movingTetracube.applyTransformationMatrix(transformationMatrix, mat4.create(), true);
        }
        return !success;
    }

    //deletes all full slices and adds points
    emptyFullSlices() {
        for (let y = 0; y < this.collisionMatrix[0].length; y++) {
            let sliceFull = true;
            for (let x = 0; x < this.collisionMatrix.length; x++) {
                let rowFull = true;
                for (let z = 0; z < this.collisionMatrix[0][0].length; z++) {
                    if(this.collisionMatrix[x][y][z] < 2){
                        rowFull = false;
                        break;
                    }
                }
                if(!rowFull){
                    sliceFull = false;
                    break;
                }
            }
            if(sliceFull){
                this.gameManager.addPoint();
                this.clearSlice(y);
            }
        }
    }

    //clears one slice and moves all cubes above that slice one cube down
    clearSlice(sliceY) {
        for (let i = 0; i < this.fixedTetracubes.length; i++) {
            this.fixedTetracubes[i].clearSlice(sliceY, this.gameManager.unitManager.unitDimensions);
        }
        for (let y = sliceY; y > 0; y--) {
            for(let x = 0; x < this.collisionMatrix.length; x++){
                for(let z = 0; z < this.collisionMatrix[0][0].length; z++){
                    this.collisionMatrix[x][y][z] = this.collisionMatrix[x][y - 1][z];
                }
            }
        }
    }

    createTetracube(cubeType) {
        let cubes = new Array();
        let topCenter = this.gameManager.unitManager.getTopCenter();

        let startPosition = vec3.create();
        let axis = vec3.create();
        let numberOfCubes = 0;
        let color = [1, 1, 1, 1];
        let rotationCubeIndex = 0;
        let rotationCubeVertexIndex = 0;

        switch (cubeType) {

            case tetracubeTypes.I:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(2, 0, 0));
                numberOfCubes = 4;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.O:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 0, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.L:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 3;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(-1, 0, 0));
                numberOfCubes = 1;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.T:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 3;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(0, 0, 0));
                numberOfCubes = 1;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.N:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(0, 0, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.TR:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                axis = vec3.fromValues(0, 1, 0);
                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(0, 1, 1));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.TL:
                color = this.generateRandomColor(); 
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 0));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                axis = vec3.fromValues(0, 1, 0);
                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 1));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;

            case tetracubeTypes.TRI:
                color = this.generateRandomColor();
                axis = vec3.fromValues(1, 0, 0);
                rotationCubeIndex = 2;

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(0, 1, 0));
                numberOfCubes = 1;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(1, 1, 1));
                numberOfCubes = 2;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                startPosition = vec3.subtract(vec3.create(), topCenter, vec3.fromValues(0, 0, 1));
                numberOfCubes = 1;
                cubes = cubes.concat(this.createUnitCubes(startPosition, axis, numberOfCubes, color));

                break;
        }

        return new Tetracube(this, cubeType, cubes, rotationCubeIndex, rotationCubeVertexIndex);
    }

    generateRandomColor(){
        return vec4.fromValues(Math.random() * 0.8 + 0.2, Math.random() * 0.8 + 0.2, Math.random() * 0.8 + 0.2, 1);
    }

    createUnitCubes(startPosition, axis, numberOfCubes, color) {
        let cubes = new Array();
        for (let i = 0; i < numberOfCubes; i++) {
            let centerPosition = vec3.add(vec3.create(), startPosition, vec3.scale(vec3.create(), axis, i));
            cubes.push(this.createUnitCube(centerPosition, color));
        }
        return cubes;
    }

    //create one unit cube
    createUnitCube(center, color) {
        let cubePositions = [
            // Front face
            center[0] - 0.5, center[1] - 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] + 0.5,

            center[0] - 0.5, center[1] - 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] + 0.5, // = collision matrix index one

            // Back face
            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] - 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] - 0.5, // = collision matrix index two

            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] - 0.5,

            // Top face
            center[0] - 0.5, center[1] + 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] + 0.5,

            center[0] - 0.5, center[1] + 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] - 0.5,

            // Bottom face
            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] + 0.5,
            center[0] - 0.5, center[1] - 0.5, center[2] + 0.5,

            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] - 0.5,

            // Right face
            center[0] + 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] - 0.5, center[2] + 0.5,

            center[0] + 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] + 0.5, center[1] + 0.5, center[2] - 0.5,

            // Left face
            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] - 0.5, center[1] - 0.5, center[2] + 0.5,

            center[0] - 0.5, center[1] - 0.5, center[2] - 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] + 0.5,
            center[0] - 0.5, center[1] + 0.5, center[2] - 0.5,
        ];

        let cubeNormals = [
            // Front face
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,

            // Back face
            0.0, 0.0, -1.0,
            0.0, 0.0, -1.0,
            0.0, 0.0, -1.0,

            0.0, 0.0, -1.0,
            0.0, 0.0, -1.0,
            0.0, 0.0, -1.0,

            // Top face
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,

            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,

            // Bottom face
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,

            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, -1.0, 0.0,

            // Right face
            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0,

            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0,

            // Left face
            -1.0, 0.0, 0.0,
            -1.0, 0.0, 0.0,
            -1.0, 0.0, 0.0,

            -1.0, 0.0, 0.0,
            -1.0, 0.0, 0.0,
            -1.0, 0.0, 0.0,
        ];

        let cubeColors = new Array();

        for (let i = 0; i < 36; i++) {
            cubeColors = cubeColors.concat(Array.prototype.slice.call(color));
        }

        return new Cube(cubePositions, cubeColors, cubeNormals);
    }

}