class Cube {
    constructor(positions, colors, normals) {
        this.positions = positions;
        this.colors = colors;
        this.normals = normals;
    }

    getData(){
        return {
            colors: this.colors,
            positions: this.positions,
            normals: this.normals
        }
    }

    applyTransformationMatrix(matrix, rotationMatrix){
        if(this.positions && this.normals){
            for(let i = 0; i < this.positions.length; i += 3){

                let positionVector = vec3.fromValues(this.positions[i], this.positions[i + 1], this.positions[i + 2]);
                let normalVector = vec3.fromValues(this.normals[i], this.normals[i + 1], this.normals[i + 2]);
    
                vec3.transformMat4(positionVector, positionVector, matrix);
                vec3.transformMat4(normalVector, normalVector, rotationMatrix);
    
                this.positions[i] = positionVector[0];
                this.positions[i + 1] = positionVector[1];
                this.positions[i + 2] = positionVector[2];
    
                this.normals[i] = normalVector[0];
                this.normals[i + 1] = normalVector[1];
                this.normals[i + 2] = normalVector[2];
            }
        }
    }

    getPoint(startIndex){
        return vec3.fromValues(this.positions[startIndex], this.positions[startIndex + 1], this.positions[startIndex + 2]);
    }

    removeData(){
        this.positions.length = 0;
        this.colors.length = 0;
        this.normals.length = 0;
    }
}
