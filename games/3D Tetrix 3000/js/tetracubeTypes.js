//this "enum" notes the possible tetracubes
const tetracubeTypes = Object.freeze({"I":1, "O":2, "L":3, "T":4, "N":5, "TR":6, "TL":7, "TRI":8});