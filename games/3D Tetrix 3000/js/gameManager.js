class GameManager {
    constructor() {
        this.ambient = 0.1;
        this.diffuse = 0.8;
        this.specular = 0.2;

        this.points = 0;

        this.sizeDivider = 10;
        this.tetracubeRotating = false;
        this.unitSizes = vec3.fromValues(4 * (1 / this.sizeDivider), 1 / this.sizeDivider, 4 * (1 / this.sizeDivider));
        this.gameRatio = this.unitSizes[0] / this.unitSizes[1];

        this.freeMovement = false;
        this.cheating = this.freeMovement;
        this.advancedGridEnabled = false;
        this.gravityEnabled = true;
        this.gravityForce = 1; //in cubes per second
        this.defaultGravityForce = 1;

        this.intializeViewingMatrices();
        this.initializeWebGL();
        this.aspectRatio = this.webGLData.context.canvas.clientWidth / this.webGLData.context.canvas.clientHeight;
        let rotationAxis = vec3.fromValues(1, 0 , 0);
        this.rotateCamera(rotationAxis);

        this.unitManager = new UnitManager(this.unitSizes, this.aspectRatio);
        this.tetracubeManager = new TetracubeManager(this);
        this.spawnTetracube();
        this.inputManager = new InputManager(this);

        this.lastRenderTime = (new Date()).getTime();
        this.defaultSecondsPerRotation = 1;
        this.secondsPerRotation = 1;

        this.nextRotations = new Array();
        this.initializeTestButtons();
        this.accumulatedYTranslation = 1;

        this.grid = this.unitManager.createGrid(false);
        this.advancedGrid = this.unitManager.createGrid(true);

        this.render();
    }

    gameOver(){
        if(this.cheating){
            confirm("You scored " + this.points + " Points, that'd be great and all, but you cheated.");
        } else{
            if(this.points < 10){
                confirm("You scored " + this.points + " Points. You can do better, try again!");
            } else if(this.points < 15){
                confirm("Good! You scored " + this.points + " Points.");
            } else if(this.points < 20){
                confirm("Great! You scored " + this.points + " Points.");
            } else{
                confirm("Wow! You scored " + this.points + " Points, that's really good!");
            }            
        }
        this.restartGame();
    }

    //restarts the game
    restartGame() {
        //the most important parts get reassigned
        this.unitManager = new UnitManager(this.unitSizes, this.aspectRatio);
        this.tetracubeManager = new TetracubeManager(this);
        this.inputManager.setGameManager(this);
        this.spawnTetracube();
        this.freeMovement = false;
        this.cheating = false;
        this.points = 0;
        this.refreshInfoTexts();
    }

    addPoint(){
        this.points++;
        this.refreshInfoTexts();
    }

    initializeWebGL() {
        let canvas = document.getElementById("webGLCanvas");
        let context = canvas.getContext('webgl');

        if (!context) {
            alert('Unable to initialize WebGL. Your browser or machine may not support it.');
            return;
        }

        let vertexShaderSource = document.getElementById("vertexShader.glsl").textContent;
        let fragmentShaderSource = document.getElementById("fragmentShader.glsl").textContent;

        this.webGLData = new WebGLData(context, vertexShaderSource, fragmentShaderSource, this.modelViewMatrix, this.currentProjectionMatrix, this.modelViewTranslationV, this.ambient, this.diffuse, this.specular);
    }

    addRotation(axis) {
        let rotation = {
            axis: axis,
            rotation: 90,
        }
        this.nextRotations.push(rotation);
    }

    render() {
        //the needed rotation is calculated with the time that has bygone since the last frame and the rotation that should be done (which is saved inside the input manager)
        let bygoneTime = (new Date()).getTime() - this.lastRenderTime;
        this.lastRenderTime = (new Date).getTime();

        let rotationM = mat4.create();
        let finishRotation = false;

        let spawnNewBlockFlag = false;

        if (this.nextRotations.length > 0) {
            let nextRotationValue = this.nextRotations[0].rotation;
            let nextRotationAxis = this.nextRotations[0].axis;
            let toRotate = 90 * (bygoneTime / (1000 * this.secondsPerRotation));

            this.nextRotations[0].rotation -= toRotate;

            if (this.nextRotations[0].rotation < 0) {
                toRotate -= this.nextRotations[0].rotation;
                this.nextRotations.shift();
                if (this.nextRotations.length == 0) {
                    finishRotation = true;
                }
            }

            rotationM = mat4.fromRotation(mat4.create(), toRotate * Math.PI / 180, nextRotationAxis);
        }

        //checks if free movement is enabled and changes the behaviour based on free movement and enabled gravity
        if (!this.freeMovement && this.gravityEnabled) {
            this.inputManager.translation[1] = -this.gravityForce * (bygoneTime / 1000);
            this.accumulatedYTranslation -= this.inputManager.translation[1];
            if(this.accumulatedYTranslation >= 1){
                if(!this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, -1, 0))){
                    this.inputManager.translation[1] = 1 - (this.accumulatedYTranslation + this.inputManager.translation[1]);
                    spawnNewBlockFlag = true;
                }
                else{
                    this.accumulatedYTranslation -= 1;
                }
            }
        }

        let rotationCenter = this.tetracubeManager.movingTetracube.getRotationCenter();
        let translateToCenterM = mat4.fromTranslation(mat4.create(), vec3.negate(vec3.create(), rotationCenter));
        let translateToTargetV = vec3.add(vec3.create(), rotationCenter, this.inputManager.translation);
        let translateToTargetM = mat4.fromTranslation(mat4.create(), translateToTargetV);

        let transformationMatrix = mat4.multiply(mat4.create(), translateToTargetM, mat4.multiply(mat4.create(), rotationM, translateToCenterM));

        //the transformations are applied on the moving block
        this.tetracubeManager.movingTetracube.applyTransformationMatrix(mat4.fromTranslation(mat4.create(), this.inputManager.translation), mat4.create(), true);
        this.tetracubeManager.movingTetracube.applyTransformationMatrix(transformationMatrix, rotationM, false);

        this.inputManager.translation = vec3.create();

        if (finishRotation) {
            this.tetracubeManager.movingTetracube.finishRotation(vec3.fromValues(0, 0, 0));
        }

        this.updateGameScreen();

        if(spawnNewBlockFlag){
            this.spawnTetracube();
        }

        requestAnimationFrame(this.render.bind(this));
    }

    updateGameScreen() {
        let tetracubeData = this.tetracubeManager.getTetracubeData();
        let buffers = this.webGLData.updateBuffers(tetracubeData);
        this.webGLData.drawTriangles(buffers, tetracubeData.vertexCount);

        if(this.advancedGridEnabled){
            buffers = this.webGLData.updateBuffers(this.advancedGrid);
            this.webGLData.drawLines(buffers, this.advancedGrid.vertexCount);
        }
        else{
            buffers = this.webGLData.updateBuffers(this.grid);
            this.webGLData.drawLines(buffers, this.grid.vertexCount);
        }
    }

    spawnTetracube(){
        this.spawning = true;

        if(this.tetracubeManager.movingTetracube){
            this.tetracubeManager.movingTetracube.finishRotation(vec3.fromValues(0, 0, 0));
            this.tetracubeManager.fixTetracube();
        }
        this.nextRotations = new Array();
        this.accumulatedYTranslation = 1;

        //this.emptyFullRows();

        let tetracubeType = this.tetracubeManager.getSpawnableType();
        let newTetracube = this.tetracubeManager.createTetracube(tetracubeType);
        if(!this.tetracubeManager.registerInCollisionMatrix(newTetracube)){
            this.gameOver();
        }
        this.spawning = false;
    }

    intializeViewingMatrices(){
        let fieldOfView = 60 * Math.PI / 180;   // in radians
        let aspect = 1;
        let zNear = 0.1;
        let zFar = 100.0;        

        this.perspectiveMatrix = mat4.perspective(mat4.create(), fieldOfView, aspect, zNear, zFar);
        this.orthographicMatrix = mat4.ortho(mat4.create(), -2, 2, -2, 2, -10, 10);

        this.currentProjectionMatrix = this.perspectiveMatrix;
        this.perspectiveViewing = true;

        this.currentScaleV = vec3.create();
        this.currentOrientation = orientations.STANDING;
        this.modelViewTranslationV = vec3.fromValues(0, 0, -5);
        this.modelViewMatrix = mat4.fromTranslation(mat4.create(), this.modelViewTranslationV);
        this.localToGlobalCamera = mat4.create();
    }

    calculateModelViewScaling(currentOrientation, localRotationAxis){
        let rotationAxis = 0;
        if(localRotationAxis[1] != 0){
            rotationAxis = 1;
        } else if(localRotationAxis[2] != 0){
            rotationAxis = 2;
        }
        switch(currentOrientation){
            case orientations.STANDING:
                switch(rotationAxis){
                    case 0:
                        return orientations.SQUARED;
                        break;
                    case 1:
                        return orientations.STANDING;
                        break;
                    case 2:
                        return orientations.LYING;
                        break;
                }
                break;
            case orientations.LYING:
                switch(rotationAxis){
                    case 0:
                        return orientations.LYING;
                        break;
                    case 1:
                        return orientations.SQUARED
                        break;
                    case 2:
                        return orientations.STANDING
                        break;
                }
                break;
            case orientations.SQUARED:
                switch(rotationAxis){
                    case 0:
                        return orientations.STANDING;
                        break;
                    case 1:
                        return orientations.LYING;
                        break;
                    case 2:
                        return orientations.SQUARED;
                        break;
                }
                break;
        }
    }

    rotateCamera(axis){
        let localAxis = vec3.round(vec3.create(), vec3.transformMat4(vec3.create(), axis, this.localToGlobalCamera));
        this.currentOrientation = this.calculateModelViewScaling(this.currentOrientation, localAxis);

        switch(this.currentOrientation){
            case orientations.STANDING:
                this.currentScaleV = vec3.fromValues(1, this.gameRatio / 2, 1);
                break;
            case orientations.LYING:
                this.currentScaleV = vec3.fromValues(this.gameRatio / 2, 1 / this.gameRatio, 1);
                break;
            case orientations.SQUARED:
                this.currentScaleV = vec3.fromValues(1, this.aspectRatio, 1);
                break;
        }

        let rotationM = mat4.fromRotation(mat4.create(), 90 * Math.PI / 180, localAxis);
        this.localToGlobalCamera = mat4.multiply(mat4.create(), mat4.fromRotation(mat4.create(), 90 * Math.PI / 180, vec3.negate(vec3.create(), localAxis)), this.localToGlobalCamera);

        this.updateViewing(rotationM, this.modelViewTranslationV);
    }

    zoom(zoomTranslation){
        let centerTranslation = this.modelViewTranslationV;
        this.modelViewTranslationV = vec3.add(vec3.create(), this.modelViewTranslationV, vec3.fromValues(0, 0, zoomTranslation));
        let rotationM = mat4.create();

        this.updateViewing(rotationM, centerTranslation);
    }

    updateViewing(rotationM, centerTranslation){
        let translateToCenterM = mat4.fromTranslation(mat4.create(), vec3.negate(vec3.create(), centerTranslation));

        let translateBackM = mat4.fromTranslation(mat4.create(), this.modelViewTranslationV);

        let scaleM = mat4.fromScaling(mat4.create(), this.currentScaleV);

        let modelViewRotationMatrix =  mat4.multiply(mat4.create(), rotationM, mat4.multiply(mat4.create(), translateToCenterM, this.modelViewMatrix));
        this.modelViewMatrix = mat4.multiply(mat4.create(), translateBackM, modelViewRotationMatrix);
        this.scaledModelViewMatrix = mat4.multiply(mat4.create(), scaleM, this.modelViewMatrix);

        this.webGLData.updateViewing(this.scaledModelViewMatrix, this.currentProjectionMatrix, vec3.negate(vec3.create(), centerTranslation));
    }

    changePerspective(){
        if(this.perspectiveViewing){
            this.currentProjectionMatrix = this.orthographicMatrix;
        } else{
            this.currentProjectionMatrix = this.perspectiveMatrix;
        }
        this.perspectiveViewing = !this.perspectiveViewing;

        let rotationM = mat4.create();

        this.updateViewing(rotationM, this.modelViewTranslationV);
    }

    updateMaterials(ambient, diffuse, specular){
        this.webGLData.updateMaterials(ambient, diffuse, specular);
    }

    //the test buttons are just for debugging and testing
    initializeTestButtons() {
        //cube rotation
        document.getElementById("rotateClockwiseX").addEventListener("click", function () {
            let axis = [1, 0, 0];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));
        document.getElementById("rotateClockwiseY").addEventListener("click", function () {
            let axis = [0, 1, 0];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));
        document.getElementById("rotateClockwiseZ").addEventListener("click", function () {
            let axis = [0, 0, 1];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));
        document.getElementById("rotateCounterClockwiseX").addEventListener("click", function () {
            let axis = [-1, 0, 0];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));
        document.getElementById("rotateCounterClockwiseY").addEventListener("click", function () {
            let axis = [0, -1, 0];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));
        document.getElementById("rotateCounterClockwiseZ").addEventListener("click", function () {
            let axis = [0, 0, -1];
            if (this.tetracubeManager.makeRotationMovement(axis)) {
                this.addRotation(axis);
            }
        }.bind(this));

        //camera rotation
        document.getElementById("rotateCamClockwiseX").addEventListener("click", function () {
            let rotationAxis = [1, 0, 0];
            this.rotateCamera(rotationAxis);
        }.bind(this));
        document.getElementById("rotateCamClockwiseY").addEventListener("click", function () {
            let rotationAxis = [0, 1, 0];
            this.rotateCamera(rotationAxis);
        }.bind(this));
        document.getElementById("rotateCamClockwiseZ").addEventListener("click", function () {
            let rotationAxis = [0, 0, -1];
            this.rotateCamera(rotationAxis);
        }.bind(this));
        document.getElementById("rotateCamCounterClockwiseX").addEventListener("click", function () {
            let rotationAxis = [-1, 0, 0];
            this.rotateCamera(rotationAxis);
        }.bind(this));
        document.getElementById("rotateCamCounterClockwiseY").addEventListener("click", function () {
            let rotationAxis = [0, -1, 0];
            this.rotateCamera(rotationAxis);
        }.bind(this));
        document.getElementById("rotateCamCounterClockwiseZ").addEventListener("click", function () {
            let rotationAxis = [0, 0, 1];
            this.rotateCamera(rotationAxis);
        }.bind(this));

        //zoom camera
        document.getElementById("zoomCameraIn").addEventListener("click", function () {
            this.zoom(1);
        }.bind(this));
        document.getElementById("zoomCameraOut").addEventListener("click", function () {
            this.zoom(-1);
        }.bind(this));

        //switch projection
        document.getElementById("switchProjection").addEventListener("click", function () {
            this.changePerspective();
        }.bind(this));

        //translation
        document.getElementById("translateX").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(1, 0, 0), true)) {
                this.inputManager.translation[0] += 1;
            }
        }.bind(this));
        document.getElementById("translateY").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 1, 0), true)) {
                this.inputManager.translation[1] += 1;
            }
        }.bind(this));
        document.getElementById("translateZ").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 0, 1), true)) {
                this.inputManager.translation[2] += 1;
            }
        }.bind(this));
        document.getElementById("translateMinusX").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(-1, 0, 0), true)) {
                this.inputManager.translation[0] -= 1;
            }
        }.bind(this));
        document.getElementById("translateMinusY").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, -1, 0), true)) {
                this.inputManager.translation[1] -= 1;
            }
        }.bind(this));
        document.getElementById("translateMinusZ").addEventListener("click", function () {
            if (this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 0, -1), true)) {
                this.inputManager.translation[2] -= 1;
            }
        }.bind(this));

        document.getElementById("pauseGravity").addEventListener("click", function () {
            this.cheating = true;
            this.gravityEnabled = !this.gravityEnabled;
        }.bind(this));
        document.getElementById("toggleGrid").addEventListener("click", function () {
            this.advancedGridEnabled = !this.advancedGridEnabled;
        }.bind(this));
        document.getElementById("showHideButton").addEventListener("click", function () {
            let controls = document.getElementById("buttons");
            if (controls.style.display === "none") {
                controls.style.display = "block";
            } else {
                controls.style.display = "none";
            }
        }.bind(this));
        document.getElementById("showTutorial").addEventListener("click", function () {
            let tutorial = document.getElementById("tutorial");
            if (tutorial.style.display === "none") {
                tutorial.style.display = "block";
            } else {
                tutorial.style.display = "none";
            }
        }.bind(this));
        //material options
        document.getElementById("ambientSlider").addEventListener("input", function (value) {
            this.ambient = document.getElementById("ambientSlider").value / 100.0;
            this.updateMaterials(this.ambient, this.diffuse, this.specular);
            this.refreshInfoTexts();
        }.bind(this));
        document.getElementById("diffuseSlider").addEventListener("input", function (value) {
            this.diffuse = document.getElementById("diffuseSlider").value / 100.0;
            this.updateMaterials(this.ambient, this.diffuse, this.specular);
            this.refreshInfoTexts();
        }.bind(this));
        document.getElementById("specularSlider").addEventListener("input", function (value) {
            this.specular = document.getElementById("specularSlider").value / 100;
            this.updateMaterials(this.ambient, this.diffuse, this.specular);
            this.refreshInfoTexts();
        }.bind(this));


        let restartButtons = document.getElementsByClassName("restart");
        for (let i = 0; i < restartButtons.length; i++) {
            restartButtons[i].addEventListener("click", function () {
                this.restartGame();
            }.bind(this));
        }
        document.getElementById("rotationSpeedSlider").addEventListener("input", function (value) {
            this.secondsPerRotation = this.defaultSecondsPerRotation * (document.getElementById("rotationSpeedSlider").value * 0.01);
            this.refreshInfoTexts();
        }.bind(this));
        document.getElementById("gravitySpeedSlider").addEventListener("input", function (value) {
            this.cheating = true;
            this.gravityForce = this.defaultGravityForce * (document.getElementById("gravitySpeedSlider").value * 0.01);
            this.refreshInfoTexts();
        }.bind(this));
        document.getElementById("freeMovementCheckbox").addEventListener("change", function (value) {
            this.cheating = true;
            if (document.getElementById("freeMovementCheckbox").checked) {
                this.freeMovement = true;
            } else {
                this.freeMovement = false;
            }
        }.bind(this));

        this.secondsPerRotation = this.defaultSecondsPerRotation * (document.getElementById("rotationSpeedSlider").value * 0.01);
        this.gravityForce = this.defaultGravityForce * (document.getElementById("gravitySpeedSlider").value * 0.01);
        document.getElementById("freeMovementCheckbox").checked = this.freeMovement;
        let tutorial = document.getElementById("tutorial");
        tutorial.style.display = "none";
        this.refreshInfoTexts();
    }
    //changes the info texts to the current data
    refreshInfoTexts() {
        if (this.gravityForce == 1) {
            document.getElementById("gravitySliderText").innerHTML = "1 cube per second";
        }
        else {
            document.getElementById("gravitySliderText").innerHTML = Math.round(this.gravityForce * 100) / 100 + " cubes per second";
        }
        if (this.secondsPerRotation == 1) {
            document.getElementById("rotationSliderText").innerHTML = "1 second";
        }
        else {
            document.getElementById("rotationSliderText").innerHTML = Math.round(this.secondsPerRotation * 100) / 100 + " seconds";
        }
        document.getElementById("ambientText").innerHTML = this.ambient;
        document.getElementById("diffuseText").innerHTML = this.diffuse;
        document.getElementById("specularText").innerHTML = this.specular;
        document.getElementById("points").innerHTML = this.points;
    }
}