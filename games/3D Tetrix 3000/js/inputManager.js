class InputManager {
    //the input manager reads the input, checks if they are possible and saves the rotation and translation
    constructor(gameManager){
        this.translation = vec3.create();
        this.gameManager = gameManager;
        this.tetracubeManager = this.gameManager.tetracubeManager;

        this.keyHandler = function(event){
            if (event.keyCode == 68 || event.keyCode == 39) { //= d || arrow right
                if(this.tetracubeManager.makeCollisionMovement(vec3.fromValues(1, 0, 0), true)){
                    this.translation[0] += 1;
                }
            }
            if (event.keyCode == 65 || event.keyCode == 37) { //= a || arrow left
                if(this.tetracubeManager.makeCollisionMovement(vec3.fromValues(-1, 0, 0), true)){
                    this.translation[0] -= 1;
                }
            }
            if (event.keyCode == 87 || event.keyCode == 38) { //= w || arrow up
                if(this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 0, 1), true)){
                    this.translation[2] += 1;
                }
            }
            if (event.keyCode == 83 || event.keyCode == 40) { //= s || arrow down
                if(this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 0, -1), true)){
                    this.translation[2] -= 1;
                }
            }            
            if (event.keyCode == 82) { //= r
                if(this.gameManager.freeMovement && this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, 1, 0), true)){
                    this.translation[1] += 1;
                }
            }
            if (event.keyCode == 70) { //= f
                if(this.gameManager.freeMovement && this.tetracubeManager.makeCollisionMovement(vec3.fromValues(0, -1, 0), true)){
                    this.translation[1] -= 1;
                }
            }

            //this method of detecting capital letters is not working in safari or opera
            let capital = true;
            let shift = event.getModifierState("Shift");
            let capsLock = event.getModifierState("CapsLock");
            let axis = [0, 0, 0];

            if((!shift && !capsLock) || (shift && capsLock)){
                capital = false;
            }

            if (event.keyCode == 88){ //= x
                if(capital){ //rotate clockwise
                    axis = [-1, 0, 0];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                } else{ //rotate counterclockwise
                    axis = [1, 0, 0];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                }
            }
            if (event.keyCode == 89){ //= y
                if(capital){ //rotate clockwise
                    axis = [0, -1, 0];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                } else{ //rotate counterclockwise
                    axis = [0, 1, 0];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                }
            }
            if (event.keyCode == 90){ //= z
                if(capital){ //rotate clockwise
                    axis = [0, 0, -1];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                } else{ //rotate counterclockwise
                    axis = [0, 0, 1];
                    if(this.tetracubeManager.makeRotationMovement(axis)){
                        this.gameManager.addRotation(axis);
                    }
                }
            }
            if (event.keyCode == 71){ //= g
                this.gameManager.advancedGridEnabled = !this.gameManager.advancedGridEnabled;
            }
            if (event.keyCode == 80){ //= p
                this.gameManager.gravityEnabled = !this.gameManager.gravityEnabled;
                this.gameManager.cheating = true;
            }
            if(event.keyCode == 32){ //= space
                this.gameManager.tetracubeManager.moveToBottom();
            }

            if(event.keyCode == 74){ //= j
                let rotationAxis = [0, -1, 0];
                this.gameManager.rotateCamera(rotationAxis);
            }
            if(event.keyCode == 76){ //= l
                let rotationAxis = [0, 1, 0];
                this.gameManager.rotateCamera(rotationAxis);
            }
            if(event.keyCode == 73){ //= i
                let rotationAxis = [-1, 0, 0];
                this.gameManager.rotateCamera(rotationAxis);
            }
            if(event.keyCode == 75){ //= k
                let rotationAxis = [1, 0, 0];
                this.gameManager.rotateCamera(rotationAxis);
            }
            if(event.keyCode == 85){ //= u
                let rotationAxis = [0, 0, 1];
                this.gameManager.rotateCamera(rotationAxis);
            }
            if(event.keyCode == 79){ //= o
                let rotationAxis = [0, 0, -1];
                this.gameManager.rotateCamera(rotationAxis);
            }

            if(event.keyCode == 171 || event.keyCode == 107){ //= + || add
                this.gameManager.zoom(1);
            }
            if(event.keyCode == 173 || event.keyCode == 109){ //= - || substract
                this.gameManager.zoom(-1);
            }
            if(event.keyCode == 86){ //= v
                this.gameManager.changePerspective();
            }

        }

        document.addEventListener("keydown", this.keyHandler.bind(this), false);
    }

    setGameManager(gameManager){
        this.translation = vec3.create();
        this.gameManager = gameManager;
        this.tetracubeManager = this.gameManager.tetracubeManager;
    }
}